import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  @Output()
  sortAction: EventEmitter<SortType> = new EventEmitter<SortType>();

  @Output()
  filterAction: EventEmitter<string> = new EventEmitter<string>();

  filterValue = new FormControl('');

  public SortTypes: SortType[] = [new SortType(SortType.Name, true), new SortType(SortType.Office)];

  constructor() { }

  ngOnInit(): void {
  }

  public sortBy(type: SortType): void {
    this.select(type);
    this.sortAction.emit(type)
  }

  public filter(): void {
    this.filterAction.emit(this.filterValue.value);
  }

  public onKey(e: any){
    if(e.keyCode === 13){
       this.filter();
    }
 }

  private select(type: SortType): void {
    this.SortTypes.forEach(t => {
      t.isSelected = false;
    })
    type.isSelected = true;
  }
}

export class SortType {

  public static Name = 'Name';
  public static Office = 'Office'

  constructor(public type: string,
    public isSelected = false) {}
}
