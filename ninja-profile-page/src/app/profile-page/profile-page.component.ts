import { Component, OnInit, Output } from '@angular/core';
import { DataHandlerService, NinjaProfile } from '../data-handler/data-handler.service';
import { SortType } from './toolbar/toolbar.component';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit {


  constructor(
    private dataHandler: DataHandlerService
  ) { }

  @Output()
  filteredProfiles: NinjaProfile[] = [];

  ngOnInit(): void {
    this.dataHandler.downloadData().subscribe(isLoadingDone => {
      if (isLoadingDone){
        this.filteredProfiles = this.dataHandler.getData();
      }
    })
  }

  public sortBy(type: SortType){
    this.filteredProfiles = this.dataHandler.getData(undefined, type.type);
  }

  public filterBy(value: string){
    this.filteredProfiles = this.dataHandler.getData(value);
  }

  public loadMore(){
    this.filteredProfiles = this.dataHandler.loadMore();
  }
}
