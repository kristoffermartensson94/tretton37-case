import { Component, Input, OnInit } from '@angular/core';
import { NinjaProfile } from 'src/app/data-handler/data-handler.service';

@Component({
  selector: 'app-profile-grid',
  templateUrl: './profile-grid.component.html',
  styleUrls: ['./profile-grid.component.scss']
})
export class ProfileGridComponent implements OnInit {

  constructor() { }

  @Input()
  profiles: NinjaProfile[] = []

  ngOnInit(): void {}

  public goToLinkedIn(name: string): void {
    window.open("https://www.linkedin.com" + name, "_blank");
  }

  public goToGithub(name: string): void {
    window.open("https://www.github.com/" + name, "_blank");
  }

  public goToTwitter(name: string): void {
    window.open("https://www.twitter.com/" + name, "_blank");
  }

}
