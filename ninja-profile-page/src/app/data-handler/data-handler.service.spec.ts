import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing'

import { DataHandlerService, NinjaProfile } from './data-handler.service';
import { SortType } from '../profile-page/toolbar/toolbar.component';

let mockProfile1: NinjaProfile = {
  email: 'test1@test.com',
  gitHub: 'githubname1',
  highlighted: false,
  imagePortraitUrl: 'imgsrc1',
  imageWallOfLeetUrl: 'imgurl1',
  linkedIn: 'linkedinname1',
  mainText: 'text1',
  manager: 'manager1',
  name: 'Bname1',
  office: 'office2',
  orgUnit: 'org1',
  phoneNumber: '1',
  published: false,
  stackOverflow: 'stackovername1',
  twitter: 'twittername1'
}

let mockProfile2: NinjaProfile = {
  email: 'test2@test.com',
  gitHub: 'githubname2',
  highlighted: false,
  imagePortraitUrl: 'imgsrc2',
  imageWallOfLeetUrl: 'imgurl2',
  linkedIn: 'linkedinname2',
  mainText: 'text2',
  manager: 'manager2',
  name: 'Cname2',
  office: 'office1',
  orgUnit: 'org2',
  phoneNumber: '2',
  published: false,
  stackOverflow: 'stackovername2',
  twitter: 'twittername2'
}

let mockProfile3: NinjaProfile = {
  email: 'test3@test.com',
  gitHub: 'githubname3',
  highlighted: false,
  imagePortraitUrl: 'imgsrc3',
  imageWallOfLeetUrl: 'imgurl3',
  linkedIn: 'linkedinname3',
  mainText: 'text3',
  manager: 'manager3',
  name: 'Aname3',
  office: 'office2',
  orgUnit: 'org3',
  phoneNumber: '3',
  published: false,
  stackOverflow: 'stackovername3',
  twitter: 'twittername3'
}

describe('DataHandlerService', () => {
  let service: DataHandlerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(DataHandlerService);
    service.data = [mockProfile1, mockProfile2, mockProfile3];
    service.filteredData = service.data;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should sort profiles on name', () => {
    const returnedData = service.getData(undefined, SortType.Name);
    expect(returnedData[0].name).toEqual(mockProfile3.name)
    expect(returnedData[1].name).toEqual(mockProfile1.name)
    expect(returnedData[2].name).toEqual(mockProfile2.name)
  });

  it('should sort profiles on office', () => {
    const returnedData = service.getData(undefined, SortType.Office);
    console.log(returnedData)
    expect(returnedData[0].office).toEqual(mockProfile2.office)
    expect(returnedData[1].office).toEqual(mockProfile1.office)
    expect(returnedData[2].office).toEqual(mockProfile3.office)
  });

  it('should only return filtered out profile', () => {
    expect(service.getData('Aname3')[0].name).toEqual(mockProfile3.name)
  });

  it('should return filtered and sorted profiles', () => {
    const returnedData = service.getData('office2', SortType.Name);
    expect(returnedData[0].name).toEqual(mockProfile3.name)
    expect(returnedData[1].name).toEqual(mockProfile1.name)
  });

  it('should only return 5 objects', () => {
    service.data.push(mockProfile1, mockProfile2, mockProfile3)
    const returnedData = service.getData();
    expect(returnedData.length).toEqual(5);
  });

  it('should first return 5 objects then 10 objects', () => {
    service.data.push(mockProfile1, mockProfile2, mockProfile3, mockProfile1, mockProfile2, mockProfile3, mockProfile1, mockProfile2, mockProfile3)
    let returnedData = service.getData();
    expect(returnedData.length).toEqual(5);
    returnedData = service.loadMore();
    expect(returnedData.length).toEqual(10);
  });
});
