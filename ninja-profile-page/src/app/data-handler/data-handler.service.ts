import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';
import { SortType } from '../profile-page/toolbar/toolbar.component';

@Injectable({
  providedIn: 'root'
})
export class DataHandlerService {

  constructor(private http: HttpClient) {}

  public data: NinjaProfile[] = [];
  public filteredData: NinjaProfile[] = [];

  private loadingFinished = new Subject<boolean>();
  private currentSortType = SortType.Name;
  private currentFilter = '';
  private renderAmount = 8;

  public downloadData(): Subject<boolean> {
    const options = {
      headers: new HttpHeaders('Authorization: api-key 14:2021-09-08:anna.vanduijvenbode@tretton37.com eb945dd2d02ebaba40402a32f0ce0170a32898ba5ea8f4482d6b87ac5d22135f')
    }
    this.http.get('https://api.1337co.de/v3/employees', options).subscribe(result => {
      this.handleDataResult(result);
    });
    return this.loadingFinished;
  }

  public getData(filterValue = this.currentFilter, sortType = this.currentSortType): NinjaProfile[] {
    if (filterValue !== undefined && filterValue !== this.currentFilter) {
      this.currentFilter = filterValue;
      this.filterOnStringValue(filterValue);
    }
    return this.sortOnSortType(sortType);
  }

  public loadMore(): NinjaProfile[]{
    this.renderAmount = this.renderAmount + 8;
    return this.getData();
  }

  private sortOnSortType(type: string): NinjaProfile[] {
    this.currentSortType = type;
    switch (type) {
      case SortType.Name:
        return this.sortByName();
      case SortType.Office:
        return this.sortByOffice();
      default: 
        return this.filteredData.slice(0, this.renderAmount);
    }
  }

  private filterOnStringValue(value: string){
    this.filteredData = this.data.filter(profile => {
      return profile.name.toUpperCase().includes(value.toUpperCase()) || profile.office.toUpperCase().includes(value.toUpperCase());
    });
  }

  private sortByName(): NinjaProfile[] {
    this.filteredData.sort((a, b) => a.name.localeCompare(b.name));
    return this.filteredData.slice(0, this.renderAmount);
  }

  private sortByOffice(): NinjaProfile[] {
    this.filteredData.sort((a, b) => a.office.localeCompare(b.office));
    return this.filteredData.slice(0, this.renderAmount);
  }

  private handleDataResult(result: Object) {
    this.data = result as NinjaProfile[];
    this.data = this.data.filter(profile => {
      return !!profile.imagePortraitUrl;
    })
    this.filteredData = this.data;
    this.loadingFinished.next(true);
  }
}

export interface NinjaProfile {
  email: string,
  gitHub: string,
  highlighted: boolean,
  imagePortraitUrl: string,
  imageWallOfLeetUrl: string,
  linkedIn: string,
  mainText: string,
  manager: string,
  name: string,
  office: string,
  orgUnit: string,
  phoneNumber: string,
  published: boolean,
  stackOverflow: string,
  twitter: string
}
