# Ninja profile page

This website contains profiles, presented in a grid, of all the employees of the company tretton37. The profiles contain clickable links to linkedin, github and twitter if the person have supplied these accountnames. The profiles can be sorted and filtered by name and office. The website renders only a small part of the profiles initially and more can be loaded using the load more button in the bottom of the page.

To run app:

1. Clone into repository.
2. Navigate terminal to tretton37-case\ninja-profile-page.
3. Run npm install (If you don't have node/npm installed you will need to do that first before this step)
4. Run npm start.
5. Open http://localhost:4200/home.

To run unit tests:
1. Navigate terminal to tretton37-case\ninja-profile-page.
2. Run npm test.

## My solution

Contains a service for data handling, a parent component and two child components.

- data-handler.service:

I chose to use create a service to grab the data from the api and be ultimately responsible for sorting/filtering/loading additional so that the components can be used standalone with other datasources.

- profile-page.component

The parent component holding the toolbar and the grid-view. I did it this way to keep the child components unaware of each other to make them reusable in other contexts with other data. However, when I tried to create a sidenav-toolbar for my responsive design, this setup of components turned out problematic since the solutions I found all had the sidenav, or the the toolbar, actually holding the content 'behind' them so I was unable to complete that design with this setup. Chose too move forward without it due to time running out.

- toolbar.component

Child component for the toolbar in the top of application

- profile-grid.component

Child component for the grid. This could easily be replaced with profile-list.component with this parent/component design.

## My chosen features

- Sorting, filtering and load more button.

I had these three in mind when I decided to implement a service for all data handling. I knew that having one service doing this I could make these 3 work well together and create a dynamic experience using these in parallel. 

As a potential improvement here I would say that keeping names filtered via free text and having office filtering in a dropdown would make more sense.

- Responsive design.

I decided when I started to use flex layout for two reasons: I am familiar with designing websites using this module and I knew from experience that module had straightforward support to create responsive design. I am also aware that you can use @media css solutions to handle this but flex layout felt easier. I am not super happy with the reponsive design however since as I mentioned I was unable to get the sidenav to work properly. But the grid is at least responsive.

- Unit tests.

I was familiar with the angular/jasmine setup and how unit testing these applications work. Also, since the service was really the only place where I would perform functional logic, it would be pretty straightforward to cover most of the unit-testable code in just testing that service. 

- Explorer compatability (not IE11)

Pretty much came with the framework I chose. 

